﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProviderImport.Models;
using ProviderImport.Data;
using NLog;

namespace ProviderImport
{
    public static class Providers
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];
        private const string NewLine = "<br />";

        private static readonly string StrProviderTemplateId = ConfigurationManager.AppSettings["providerTemplateID"];
        private static readonly string StrProviderParentId = ConfigurationManager.AppSettings["providerParentID"];

        private static readonly string Api = ConfigurationManager.AppSettings["api"];
        private static List<Provider> _providers;
        
        private static readonly string BaseAddress = ConfigurationManager.AppSettings["BaseAddress"];
        private static readonly string Domain = ConfigurationManager.AppSettings["domain"];
        private static readonly string Username = ConfigurationManager.AppSettings["username"];
        private static readonly string Password = ConfigurationManager.AppSettings["password"];
        private static readonly string SitecoreApi = ConfigurationManager.AppSettings["SitecoreApi"];
        private static SitecoreUser _user;

        public static void GetProviderData()
        {
            #region Get Providers from Microservice
            _providers = GetProviders();
            _user = new SitecoreUser { domain = Domain, username = Username, password = Password };
            if (_providers == null) return;
            #endregion
            try
            {
                
                #region Delete Providers
                DeleteProviders();
                #endregion

                #region Import Providers from API
                Logger.Info("BPPProviderImport :: GetProviderData -> Initiated : " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                try
                {
                    var options = new ParallelOptions { MaxDegreeOfParallelism = 4 };
                    Parallel.ForEach(_providers.Where(x=>
                        !string.IsNullOrEmpty(x.EchoDoctorNumber)), options, InsertUpdateProvider);
                }
                catch (Exception ex)
                {
                    var m = new Mail()
                    {
                        From = From,
                        To = To,
                        Body = "Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + ex.Message +
                               NewLine + " ex.InnerException: " +
                               ex.InnerException + NewLine + "Full Exception: " + ex,
                        Subject = "BPPProviderImport Process Failed"
                    };
                    SendMail.SendMailMessage(m);
                    Console.WriteLine(ex.Message);
                    Logger.Error("BPPProviderImport :: GetProviderData -> : " + ex.Message.ToString());
                    Environment.Exit(0);
                }
                #endregion
            }
            catch (Exception ex)
            {
                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "BPPProviderImport :: Exception occurred while running Providers.GetProviderData() (Delete Providers)" +
                           NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                           ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "GetProviderData (delete provider) Process Failed"
                };

                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    Logger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);
                Logger.Error("Exception occurred while running Models.Providers.GetProviderData() (Delete Providers)" +
                             NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                             ex.InnerException + NewLine);
            }
        }
        private static void InsertUpdateProvider(Provider provider)
        {
            try
            {
                var strNewProvider = provider.EchoDoctorNumber.ToString();
                var array = new JObject();
                // Get Parent Item (Provider Folder)
                using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
                {
                    try
                    {
                        client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                        client.DefaultRequestHeaders.Add("url", BaseAddress);
                        client.DefaultRequestHeaders.Add("id", StrProviderParentId);
                        var children = client.GetAsync(SitecoreApi + "/GetItemById");
                        children.Wait();
                        var result = children.Result;
                        if (result.StatusCode == HttpStatusCode.OK)
                        {
                            var content = result.Content.ReadAsStringAsync().Result;
                            array = JObject.Parse(content);
                        }
                        else
                        {
                            Logger.Error("InsertUpdateProvider :: Error getting Item By ID. Status Code: " + result.StatusCode);
                            return;
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Exception getting item by ID " + e.Message);
                    }
                }
                
                var parentPath = array["ItemPath"].ToString();

                // Check to see if provider exists
                using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
                {
                    try
                    {
                        client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                        client.DefaultRequestHeaders.Add("url", BaseAddress);
                        client.DefaultRequestHeaders.Add("parentPath", parentPath);
                        client.DefaultRequestHeaders.Add("newId", strNewProvider);
                        var itemResult = client.GetAsync(SitecoreApi + "/GetItemByPath");
                        itemResult.Wait();
                        var result = itemResult.Result;

                        // does the provider exist
                        if (result.StatusCode == HttpStatusCode.NotFound)
                        {
                            Logger.Info("Adding provider (" + provider.EchoDoctorNumber + ") " + 
                                        provider.FirstName + " " + provider.LastName);
                            AddProvider(provider, strNewProvider);
                        }
                        else if (result.StatusCode == HttpStatusCode.OK)
                        {
                            Logger.Info("Updating provider (" + provider.EchoDoctorNumber + ") " + 
                                        provider.FirstName + " " +provider.LastName);
                            var returnItem = result.Content.ReadAsStringAsync().Result;
                            var returnObject = JObject.Parse(returnItem);
                            var returnId = returnObject["ItemID"].ToString();
                           UpdateProvider(provider, returnId);
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Exception ocurred getting item from path " + e.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("BPPProviderImport :: Exception in InsertUpdateProvider :: " + ex.Message);
            }
        }
        private static void AddProvider(Provider provider, string strNewProvider)
        {
            var sitecoreProvider = BuildProvider(provider);
            if (sitecoreProvider == null) return;
            using (var client = new HttpClient() { BaseAddress = new Uri(SitecoreApi) })
            {
                client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                client.DefaultRequestHeaders.Add("url", BaseAddress);
                var jsonObject = JsonConvert.SerializeObject(sitecoreProvider);
                var content = new StringContent(
                    JsonConvert.SerializeObject(jsonObject),
                    Encoding.UTF8,
                    "application/json");

                var response = client.PostAsync(SitecoreApi + "/AddProvider", content);
                response.Wait();
                var result = response.Result;
                if (result.StatusCode == HttpStatusCode.OK)
                    Logger.Info("Provider (" + strNewProvider + ") " + provider.FirstName + " " +
                                provider.LastName + " Added successfully");
                else
                    Logger.Error("Error adding provider (" + strNewProvider + ") " + provider.FirstName + " " +
                                 provider.LastName + " Error: " + result.Content);
            }
        }
        private static SitecoreProvider BuildProvider(Provider provider)
        {
            if (provider.Locations == null)
            {
                Logger.Warn("Provider: " + provider.Id + " does not have a location so skipping");
                return null;
            }
            var sitecoreProvider = new SitecoreProvider
            {
                ItemName = provider.EchoDoctorNumber.ToString(),
                TemplateID = StrProviderTemplateId,
                FirstName = provider.FirstName.ToString(),
                MiddleName = provider.MiddleName.ToString(),
                LastName = provider.LastName.ToString()
            };
            try
            {
                var gender = provider.GenderId.ToString();
                sitecoreProvider.Gender = gender == "F" ? "Female" : "Male";
                sitecoreProvider.JobTitle = provider.EchoSuffix.ToString();
                var languages = "";
                var count = 0;
                foreach (var language in provider.LanguageMapping)
                {
                    if (count == 0)
                    {
                        languages += language.Text;
                    }
                    else
                    {
                        languages += "|" + language.Text;
                    }
                    count++;
                }
                count = 0;
                sitecoreProvider.Languages= languages.ToString();
                sitecoreProvider.NPI = provider.NationalId.ToString();

                var specialties = provider.SpecialtyMapping;
                var other = "";
                foreach (var specialty in specialties)
                {
                    var spec = GetSpecialty(specialty.SpecialtyCode);
                    if (string.IsNullOrEmpty(spec))
                    {
                        spec = specialty.Text;
                    }
                    if (specialty.IsPrimary)
                    {
                        sitecoreProvider.PrimarySpecialty = spec;
                    }
                    else
                    {
                        if (count == 0)
                        {
                            other = spec;
                        }
                        else
                        {
                            other = other + "|" + spec;
                        }
                        count++;
                    }
                }
                count = 0;
                sitecoreProvider.OtherSpecialties = other;

                // from the new API. 
                sitecoreProvider.Suffix = provider.EchoSuffix.ToString();
                sitecoreProvider.Biography = provider.Introduction.ToString();
                sitecoreProvider.PracticeInformation = provider.LegalPracticeName.ToString();
                var hospitals = "";
                foreach (var hospital in provider.HospitalMapping)
                {
                    if (count == 0)
                    {
                        hospitals = hospital.HospitalName;
                    }
                    else
                    {
                        hospitals = hospitals + "|" + hospital.HospitalName;
                    }
                    count++;
                }
                count = 0;
                sitecoreProvider.HospitalAffiliations = hospitals;
                sitecoreProvider.IsAcceptingPatients = (!string.IsNullOrEmpty(provider.IsAcceptingNewPatients.ToString())) ? (provider.IsAcceptingNewPatients.ToString().ToLower().Equals("true")) ? "1" : "0" : "0";
               
                var ages = "";
                foreach (var age in provider.AgesTreatedMappings.OrderBy(x => x.OrderId))
                {
                    if (count == 0)
                    {
                        ages = age.Text;
                    }
                    else
                    {
                        ages = ages + "|" + age.Text;
                    }
                    count++;
                }
                count = 0;
                sitecoreProvider.AgesTreated = ages;
                sitecoreProvider.IsBPP = (!string.IsNullOrEmpty(provider.IsBPP.ToString())) ? (provider.IsBPP.ToString().ToLower().Equals("true")) ? "1" : "0" : "0";
                if ((!string.IsNullOrEmpty(provider.IsHide.ToString()) || !string.IsNullOrEmpty(provider.IsArchived.ToString()))
                    && ((System.Convert.ToBoolean(provider.IsHide.ToString()) == true)
                    || (System.Convert.ToBoolean(provider.IsArchived.ToString()) == true)))
                    sitecoreProvider.ExcludeFromSearch = "1";
                else
                    sitecoreProvider.ExcludeFromSearch = "0";

                var strPrimLoc = string.Empty;
                var strOthLoc = string.Empty;
                var strPrimLocLatLng = string.Empty;
                var strOthLocLatLng = string.Empty;
                //var addresses = provider.Addresses.ToList();
                
                count = 0;
                var primaryLocation = provider.Locations.FirstOrDefault(x => x.type == "Primary Address") ?? provider.Locations.FirstOrDefault(x => x.IsDefaultLocation);
                /**
                 * Put in a check for null type. 
                 */
                var otherLocations = provider.Locations.Where(x => x.type !=null&&x.type != "Primary Address").ToList();


                // Primary Location Info
                if (primaryLocation != null)
                {
                    var fax = primaryLocation.Fax;
                    var mainPhone = primaryLocation.mainPhone;
                    var admissions = primaryLocation.admissionsPhone;
                    strPrimLoc = primaryLocation.Address1 + "|" + primaryLocation.Address2 + "|" + primaryLocation.Address3 + "|" + primaryLocation.City + "|" +
                                 primaryLocation.State + "|" + primaryLocation.Zip + "|" + mainPhone + "|" + admissions + "|" + fax;
                    strPrimLocLatLng = primaryLocation.Lat + "," + primaryLocation.Lng;
                }
                // Other Locations
                foreach (var loc in otherLocations)
                {
                    var fax = loc.Fax;
                    var mainPhone = loc.mainPhone;
                    var admissions = loc.admissionsPhone;
                    if (count > 0)
                    {
                        strOthLoc += "^";

                        strOthLocLatLng += "|";
                    }

                    strOthLoc += loc.Address1 + "|" + loc.Address2 + "|" + loc.Address3 + "|" + loc.City + "|" +
                                     loc.State + "|" + loc.Zip + "|" + mainPhone + "|" + admissions + "|" + fax;
                       
                    strOthLocLatLng += loc.Lat + "," + loc.Lng;
                    count++;
                }
                
                sitecoreProvider.PrimaryLocation = strPrimLoc;
                sitecoreProvider.PrimaryLocationLatLng = strPrimLocLatLng;
                sitecoreProvider.OtherLocations = (strOthLoc.EndsWith("|")) ? strOthLoc.TrimEnd('|') : strOthLoc;
                sitecoreProvider.OtherLocationLatLngs = (strOthLocLatLng.EndsWith("|")) ? strOthLocLatLng.TrimEnd('|') : strOthLocLatLng;

                var internship = "";
                var Internship = provider.EducationMapping.FirstOrDefault(x => x.DegreeText == "Internship");
                internship = Internship==null ? "" : Internship.InstitutionText + "|" + Internship.StartYear + "|" + Internship.EndYear;
                sitecoreProvider.Internship = internship;
                var residency = "";
                var Residency = provider.EducationMapping.FirstOrDefault(x => x.DegreeText == "Residency");
                residency = Residency==null ? "" : Residency.InstitutionText + "|" + Residency.StartYear + "|" + Residency.EndYear;
                sitecoreProvider.Residency = residency;

            }
            catch (Exception e)
            {
                Logger.Error("BPPProviderImport :: BuildProvider :: EXCEPTION " , e.Message);
                Console.WriteLine(e.Message);

                Logger.Error("BPPProviderImport :: BuildProvider -> Could not update item " + sitecoreProvider.ItemName + ": " + e.ToString());
                Console.WriteLine("BPPProviderImport :: BuildProvider -> Could not update item " + sitecoreProvider.ItemName + ": " + e.ToString());

            }
            return sitecoreProvider;
        }
        private static string GetSpecialty(string code)
        {
            var bpp = new BPPEntities();
            try
            {
                var value = bpp.SpecialtyXREFs.Where(a => a.Code == code).ToList();
                return value.Count<1 ? null : value[0].Description;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception in getspecialty " + e.Message + " " + e.InnerException);
                Logger.Error("Exception in getspecialty " + e.Message + " " + e.InnerException);
                return null;
            }
        }
        private static List<Provider> GetProviders()
        {
            var providers = new List<Provider>();
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(Api);
                response.Wait();
                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    try
                    {
                        // convert it into a list of objects 
                        providers = JsonConvert.DeserializeObject<List<Provider>>(result.Content.ReadAsStringAsync().Result);
                        return providers;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Logger.Error("Exception occurred while getting Providers from microservice " +
                                     NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                                     ex.InnerException + NewLine);
                        return null; // get out of the process. We don't want to proceed if we can't get providers
                    }
                }
                else
                {
                    Logger.Error("Unable to get providers from API ", result.RequestMessage);
                    return null;
                }
            }
        }
        private static void DeleteProviders()
        {
            Logger.Info("Starting delete providers.....");
            var result = string.Empty;
            // Get Children
            var userJson = JsonConvert.SerializeObject(_user);
            using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
            {
                client.DefaultRequestHeaders.Add("user", userJson);
                client.DefaultRequestHeaders.Add("url", BaseAddress);
                client.DefaultRequestHeaders.Add("id", StrProviderParentId);
                var children = client.GetAsync(SitecoreApi + "/GetParentChildren");
                children.Wait();
                var responseMessage = children.Result;
                if (responseMessage.StatusCode == HttpStatusCode.OK)
                {
                    result = responseMessage.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    // unable to get children
                    Logger.Error("DeleteProviders :: Error getting Children from Microservice. ErrorCode: " + responseMessage.StatusCode);
                    return;
                }
                
            }
            
            var array = JArray.Parse(result);
            foreach (var c in array.Children())
            {
                var contentId = c.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemName")?.Value.ToString();
                var lastName = c.Children<JProperty>().FirstOrDefault(x => x.Name == "LastName")?.Value
                    .ToString();
                var firstName = c.Children<JProperty>().FirstOrDefault(x => x.Name == "FirstName")?.Value
                    .ToString();
                var id = c.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemID")?.Value;

                var found = _providers.Any(item => item.EchoDoctorNumber == contentId);
                if (found) continue;

                try
                {
                    //idList.Add(id);
                    using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
                    {
                        client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                        client.DefaultRequestHeaders.Add("url", BaseAddress);
                        var jsonObject = JsonConvert.SerializeObject(id);
                        var content = new StringContent(
                            JsonConvert.SerializeObject(jsonObject),
                            Encoding.UTF8,
                            "application/json");
                        var response = client.PostAsync(SitecoreApi + "/DeleteProvider", content);
                        response.Wait();
                        var responseMessage = response.Result;
                        if (!responseMessage.IsSuccessStatusCode)
                        {
                            Logger.Error("Unable to delete provider: " + firstName + " " + lastName);
                        }
                        else
                        {
                            Logger.Info(firstName + " " + lastName + " Deleted");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Logger.Error("Exception occurred while Deleting Provider: (" + id + ") " +
                                 NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                                 ex.InnerException + NewLine);
                }
            }
            
        }
        private static void UpdateProvider(Provider provider, string returnId)
        {
            // post changes to that ID
            var sitecoreProvider = BuildProvider(provider);
            if (sitecoreProvider == null) return;
            sitecoreProvider.ParentID = StrProviderParentId;

            using (var client = new HttpClient() { BaseAddress = new Uri(SitecoreApi) })
            {
                client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                client.DefaultRequestHeaders.Add("url", BaseAddress);
                client.DefaultRequestHeaders.Add("existingId", returnId);
                var objectJson = JsonConvert.SerializeObject(sitecoreProvider);
                var content = new StringContent(
                    JsonConvert.SerializeObject(objectJson),
                    Encoding.UTF8,
                    "application/json");
                var response = client.PostAsync(SitecoreApi + "/UpdateProvider", content);
                response.Wait();
                var result = response.Result;
                if (result.StatusCode == HttpStatusCode.OK)
                {
                    Logger.Info("Provider (" + provider.EchoDoctorNumber + ") " + provider.FirstName + " " +
                                provider.LastName + "updated successfully");
                }
                else
                {
                    Logger.Error("Error updating provider (" + provider.EchoDoctorNumber + ") " +
                                 provider.FirstName + " " + provider.LastName + " Error: " + result.StatusCode);
                }
            }
        }
    }
}
