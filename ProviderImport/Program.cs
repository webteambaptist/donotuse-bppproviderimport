﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Mail;
using System.Web.UI.WebControls;
using ProviderImport.Models;
using System.Net;
using NLog;
namespace ProviderImport
{
    class Program
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];
        private const string NewLine = "<br/>";
        static void Main(string[] args)
        {
            try
            {
                var config = new NLog.Config.LoggingConfiguration();
                var logFile = new NLog.Targets.FileTarget("logFile")
                {
                    FileName = $"Logs\\BPPProviderImport-{DateTime.Now:MM-dd-yyyy}.log"
                };
                config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
                NLog.LogManager.Configuration = config;

                Console.WriteLine("Import started...");
                Logger.Info("Import started...");

                Providers.GetProviderData();
                Logger.Info("BPPProviderImport :: Completed : " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                Console.WriteLine("Import complete!");
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Logger.Error("Program/Main()" + ex);
                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "BPPProviderImport :: Exception occurred while running Providers.GetProviderData() (Delete Providers)" +
                           NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                           ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "MAIN Process Failed"
                };

                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    Logger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);
                Console.WriteLine("Program Main Exception: " + ex.ToString());
                Environment.Exit(0);
            }
        }
    }
}
